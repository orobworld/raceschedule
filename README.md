# Race Schedule #

### What is this repository for? ###

This code addresses the problem of scheduling head-to-head races of pairs of participants in a tournament. Specific aspects of the problem are as follows.

* There are $N$ participants, with $N$ guaranteed to be even.
* The tournament takes $N-1$ days, with each participant racing every other participant exactly once.
* There are two lanes. No participant can be assigned to the left (more preferable) lane for more than $M$ consecutive days, and no participant can be assigned to the right (less preferable) lane for more than $M$ consecutive days.

The problem was originally [posted on Operations Research StackExchange](https://or.stackexchange.com/questions/11979/how-do-i-convert-integer-programming-to-constraint-programming). The Java code here implements a constraint programming model solved by CP Optimizer. Details appear in a [blog post](https://orinanobworld.blogspot.com/2024/04/from-ip-to-cp.html).

The code was developed using CPLEX Studio 22.1.1 but will run with at least some (most?) earlier versions.

### License ###
The code here is copyrighted by Paul A. Rubin (yours truly) and licensed under a [Creative Commons Attribution 4.0 Unported License](http://creativecommons.org/licenses/by/4.0/deed.en). 

