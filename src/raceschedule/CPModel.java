package raceschedule;

import ilog.concert.IloException;
import ilog.concert.IloIntExpr;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearIntExpr;
import ilog.cp.IloCP;

/**
 * CPModel implements a constraint programming model to schedule the tournament.
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class CPModel implements AutoCloseable {
  private final int nbCompetitors;      // number of competitors
  private final int nbDays;             // number of days
  private final int maxLeft;            // max consecutive races in left lane
  // CP objects.
  private final IloCP model;            // the CP model
  private final IloIntVar[][] opponent; // opponent[i][d] is the index of the
                                        // opponent of participant i on day d
  private final IloIntVar[][] lane;     // lane[d][i] is the lane (0 or 1) to
                                        // which competitor i is assigned on
                                        // day d
  private final IloIntVar[][] opp2;     // transpose of opponent
  private final IloIntVar[][] lane2;    // transpose of lane
  private final IloIntVar maxRight;     // maximum # of times anyone was put
                                        // in the right lane
  private final IloIntVar minRight;     // minimum # of times anyone was put
                                        // in the right lane

  /**
   * Constructor.
   * @param nCompetitors            // the number of competitors (must be even)
   * @param mxLeft                  // maximum consecutive starts in left lane
   * @throws ilog.concert.IloException  // if CPO cannot build the model
   */
  public CPModel(final int nCompetitors, final int mxLeft) throws IloException {
    // Set the parameters.
    nbCompetitors = nCompetitors;
    nbDays = nCompetitors - 1;
    maxLeft = mxLeft;
    // Initialize the model.
    model = new IloCP();
    // Initialize the model variables.
    opponent = new IloIntVar[nbCompetitors][nbDays];
    opp2 = new IloIntVar[nbDays][nbCompetitors];
    lane = new IloIntVar[nbDays][nbCompetitors];
    lane2 = new IloIntVar[nbCompetitors][nbDays];
    for (int i = 0; i < nbCompetitors; i++) {
      for (int d = 0; d < nbDays; d++) {
        opponent[i][d] = model.intVar(0, nbCompetitors - 1);
        opp2[d][i] = opponent[i][d];
        lane[d][i] = model.boolVar();
        lane2[i][d] = lane[d][i];
      }
    }
    maxRight = model.intVar(0, nbDays);
    minRight = model.intVar(0, nbDays);
    // Constraint: a participant cannot race themself.
    for (int i = 0; i < nbCompetitors; i++) {
      for (int d = 0; d < nbDays; d++) {
        model.add(model.neq(opponent[i][d], i));
      }
    }
    // Constraint: in any race, a participant is the opponent of their opponent.
    for (int d = 0; d < nbDays; d++) {
      model.add(model.inverse(opp2[d], opp2[d]));
    }
    // Constraint: no participant races the same opponent twice.
    for (int i = 0; i < nbCompetitors; i++) {
      model.add(model.allDiff(opponent[i]));
    }
    // In any race, the opponents are in opposite lanes.
    for (int d = 0; d < nbDays; d++) {
      for (int i = 0; i < nbCompetitors; i++) {
        model.add(model.neq(lane[d][i],
                            model.element(lane[d], opponent[i][d])));
      }
    }
    // Constraint: Limit the number of consecutive races in the left lane
    // for each participant.
    for (int i = 0; i < nbCompetitors; i++) {
      for (int d = 0; d < nbDays - maxLeft; d++) {
        // In any span of maxLeft + 1 days, i must be in lane 1 at least once
        // and in lane 0 at least once (so in lane 1 at most maxLeft days).
        IloLinearIntExpr expr = model.linearIntExpr();
        for (int t = d; t <= d + maxLeft; t++) {
          expr.addTerm(1, lane[t][i]);
        }
        model.addGe(expr, 1);
        model.addLe(expr, maxLeft);
      }
    }
    // Define maxRight and minRight.
    for (int i = 0; i < nbCompetitors; i++) {
      // Count the number of times i is in the right lane.
      IloIntExpr count = model.sum(lane2[i]);
      // The count must be between the max and min.
      model.addLe(count, maxRight);
      model.addGe(count, minRight);
    }
    // Objective: minimize the range of appearances in the right lane.
    model.addMinimize(model.diff(maxRight, minRight));
    // Symmetry breaking constraint: by permuting competitor indices, we can
    // assume that on the first day competitor indices sum to nbCompetitors - 1.
    for (int i = 0; i < nbCompetitors; i++) {
      model.addEq(opponent[i][1], nbCompetitors - 1 - i);
    }
  }

  /**
   * Solves the model.
   * @param timeLimit the run time limit in seconds
   * @return true if a solution is found
   * @throws IloException if something blows up
   */
  public boolean solve(final double timeLimit) throws IloException {
    model.setParameter(IloCP.DoubleParam.TimeLimit, timeLimit);
    return model.solve();
  }

  /**
   * Closes the model.
   */
  @Override
  public void close() {
    model.end();
  }

  /**
   * Gets the solution (assuming one exists).
   * @return a string listing the races each day
   * @throws IloException if CPO blows up
   */
  public String getSolution() throws IloException {
    StringBuilder sb = new StringBuilder();
    double[] who = new double[nbCompetitors];
    double[] where = new double[nbCompetitors];
    int[] left = new int[nbCompetitors];  // counts times competitor is on left
    for (int d = 0; d < nbDays; d++) {
      sb.append("Day ").append(d + 1).append(":\n");
      boolean[] used = new boolean[nbCompetitors];
      // Get the vector of opponents on day d.
      model.getValues(opp2[d], who);
      // Get the vector of lane assignments.
      model.getValues(lane[d], where);
      // Loop through the competitors, adding their races.
      for (int i = 0; i < nbCompetitors; i++) {
        // Skip the competitor if already seen.
        if (!used[i]) {
          // Get the index of the opponent.
          int j = (int) Math.round(who[i]);
          // Get the lane for i.
          int w = (int) Math.round(where[i]);
          // Add the race based on who is in the left lane.
          if (w == 0) {
            left[i] += 1;
            sb.append("\t").append(i + 1).append(" - ")
              .append(j + 1).append("\n");
          } else {
            left[j] += 1;
            sb.append("\t").append(j + 1).append(" - ")
              .append(i + 1).append("\n");
          }
          // Mark j as used.
          used[j] = true;
        }
      }
    }
    // Record the left lane assignments.
    sb.append("\nLeft lane assignments by competitor:\n");
    for (int i = 0; i < nbCompetitors; i++) {
      sb.append("\t").append(i + 1).append(" -> ").append(left[i]).append("\n");
    }
    // Record the spread in left lane frequencies.
    int maxL = nbDays - (int) Math.round(model.getValue(minRight));
    int minL = nbDays - (int) Math.round(model.getValue(maxRight));
    sb.append("Range of left lane assignments is from ").append(minL)
      .append(" to ").append(maxL).append(".\n");
    return sb.toString();
  }
}
