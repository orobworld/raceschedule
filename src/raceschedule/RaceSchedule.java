package raceschedule;

import ilog.concert.IloException;

/**
 * RaceSchedule proposes a CP model to schedule races as described in a
 * question on OR Stack Exchange.
 *
 * https://or.stackexchange.com/questions/11979/how-do-i-convert-integer
 * -programming-to-constraint-programming
 *
 * @author Paul A. Rubin (rubin@msu.edu)
 */
public final class RaceSchedule {

  /**
   * Dummy constructor.
   */
  private RaceSchedule() { }

  /**
   * Creates and solves a CP model for the problem.
   * @param args the command line arguments
   */
  @SuppressWarnings({ "checkstyle:magicnumber" })
  public static void main(final String[] args) {
    // Set the tournament parameters.
    int nbCompetitors = 20;             // number of competitors
    int maxLeft = 2;                    // maximum successive races in left lane
    try (CPModel model = new CPModel(nbCompetitors, maxLeft)) {
      double tilim = 5;  // time limit in seconds
      System.out.println("Solving the model ...");
      if (model.solve(tilim)) {
        System.out.println("Success!");
        System.out.println(model.getSolution());
      } else {
        System.out.println("No solution found.");
      }
    } catch (IloException ex) {
      System.err.println(ex.getMessage());
    }
  }

}
